package yves.kams.iaga.core.ecs.world

import javafx.scene.input.KeyCode
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import yves.kams.iaga.core.ecs.systems.System
import yves.kams.iaga.core.settings.Setting

internal class WorldTest {
    private lateinit var world: World

    @BeforeEach
    fun setUp() {
        world = World(Setting().readOnlySetting)
                .registerSystem(SystemA::class)
    }

    @Test
    fun entityByTypes() {
        TODO("Not yet implemented")
    }

    @Test
    fun createEntity() {
        assertNotNull(world.createEntity(KeyCode.U))
    }

    @Test
    fun destroyEntity() {
        TODO("Not yet implemented")
    }

    @Test
    fun registerSystem() {
        world.registerSystem(SystemB::class)
        assertTrue(world.hasSystem(SystemB::class))
    }

    @Test
    fun unRegisterSystem() {
        TODO("Not yet implemented")
    }

    @Test
    fun onInit() {
        TODO("Not yet implemented")
    }

    @Test
    fun onUpdate() {
        TODO("Not yet implemented")
    }

    @Test
    fun onExit() {
        TODO("Not yet implemented")
    }

    internal class SystemA : System() {
        var number: Int = 1997
        override fun onInit() {
            println("Init A")
        }
    }

    internal class SystemB : System() {
        val test: Boolean = true

        override fun onInit() {
            println("Init B")
        }
    }

    internal class SystemC : System() {
        override fun onInit() {
            println("Init C")
        }
    }
}