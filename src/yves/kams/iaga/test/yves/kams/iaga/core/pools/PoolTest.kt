package yves.kams.iaga.core.pools

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class PoolTest {
    private lateinit var pool: Pool

    @BeforeEach
    fun setUp() {
        pool = Pool(100)
    }

    @Test
    fun execute() {
        pool.execute(1000) {
            val time = System.currentTimeMillis()

            var couter = 0
            while (System.currentTimeMillis() - time < 4000) {
                couter += 1
            }
            println("$it: $couter")
        }

        println("FINISHED")
        //pool.stop()
    }

    @Test
    internal fun name() {
        for (i in 0..32) {
            val time = System.currentTimeMillis()

            var couter = 0
            while (System.currentTimeMillis() - time < 100) {
                couter += 1
            }
            println(couter)
        }
    }
}