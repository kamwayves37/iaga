package yves.kams.iaga.app.states

import javafx.scene.canvas.GraphicsContext
import javafx.scene.paint.Color
import yves.kams.iaga.core.concurrent.Async
import yves.kams.iaga.core.settings.ReadOnlySetting
import yves.kams.iaga.core.states.GameState
import java.time.Duration

/**
 * @author yves kams
 * @since 2020-06-11
 */
class IntroState(setting: ReadOnlySetting) : GameState(setting) {

    override fun onInit() {
        //TODO("Not yet implemented")
    }

    override fun onUpdate(tps: Double) {
        //TODO("Not yet implemented")
    }

    override fun onDraw(gc: GraphicsContext) {
        super.onDraw(gc)

        gc.fill = Color.web("#2D3447")
        gc.fillRect(0.0, 0.0, screenWith, screenHeight)
    }

    override fun onExit() {
        //TODO("Not yet implemented")
    }
}