package yves.kams.iaga.app.states

import javafx.scene.canvas.GraphicsContext
import yves.kams.iaga.core.settings.ReadOnlySetting
import yves.kams.iaga.core.states.GameState

/**
 * @author yves kams
 * @since 2020-06-11
 */
class CreditState(setting: ReadOnlySetting) : GameState(setting) {

    override fun onInit() {
        //TODO("Not yet implemented")
    }

    override fun onUpdate(tps: Double) {
        TODO("Not yet implemented")
    }

    override fun onDraw(gc: GraphicsContext) {
        TODO("Not yet implemented")
    }

    override fun onExit() {
        //TODO("Not yet implemented")
    }
}