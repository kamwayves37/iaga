package yves.kams.iaga.app.states

import javafx.scene.canvas.GraphicsContext
import yves.kams.iaga.core.ecs.world.World
import yves.kams.iaga.core.settings.ReadOnlySetting
import yves.kams.iaga.core.states.GameState

/**
 * @author yves kams
 * @since 2020-06-12
 */
internal class PlayState(setting: ReadOnlySetting) : GameState(setting) {
    private val world: World = setting.gameWorld

    override fun onInit() {
        world.onInit()
    }

    override fun onUpdate(tps: Double) {
        world.onUpdate(tps)
    }

    override fun onDraw(gc: GraphicsContext) {
        super.onDraw(gc)
        world.onDraw(gc)

    }

    override fun onExit() {
        world.onExit()
    }
}