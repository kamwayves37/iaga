package yves.kams.iaga.app.tests

import javafx.scene.canvas.GraphicsContext
import javafx.scene.input.KeyCode
import javafx.scene.paint.Color
import yves.kams.iaga.app.components.*
import yves.kams.iaga.app.systems.IASystem
import yves.kams.iaga.app.systems.MovableSystem
import yves.kams.iaga.app.systems.RendererSystem
import yves.kams.iaga.core.ecs.entities.Entity
import yves.kams.iaga.core.ecs.entities.EntityFactory
import yves.kams.iaga.core.games.GameApp
import yves.kams.iaga.core.inputs.Input
import yves.kams.iaga.core.inputs.InputAction
import yves.kams.iaga.core.settings.Setting

/**
 * @author yves kams
 * @since 2020-06-15
 */
class Sample : GameApp() {

    private val SPEED_MULTIPLIER = 80.0
    private lateinit var player: Entity

    override fun initSettings(setting: Setting) {
        with(setting) {
            width = 800.0
            height = 600.0
            resizable = false
            version = "0.4"
        }
    }

    override fun initSystems() {
        getSetting().gameWorld
                .registerSystem(IASystem::class)
                .registerSystem(MovableSystem::class)
                .registerSystem(RendererSystem::class)
    }

    override fun initEntities(factory: EntityFactory) {
        val world = getSetting().gameWorld
        //
        player = world.createEntity(SampleType.PLAYER)
                .addComponent(TransformComponent(400.0, 250.0))
                .addComponent(MotionComponent())
                .addComponent(ShapeComponent())
                .addComponent(RenderableComponent(true))

        //
        factory[SampleType.IA] = {
            world.createEntity(SampleType.IA)
                    .addComponent(NeuralComponent())
                    .addComponent(TransformComponent(200.0, 300.0))
                    .addComponent(RenderableComponent(true))
        }

        //
        factory[SampleType.ENEMY] = {
            world.createEntity(SampleType.ENEMY)
                    .addComponent(IAComponent())
                    .addComponent(TransformComponent(Math.random() * getSetting().width, Math.random() * getSetting().height))
                    .addComponent(MotionComponent(SPEED_MULTIPLIER * (2 * Math.random() - 1), SPEED_MULTIPLIER * (2 * Math.random() - 1)))
                    .addComponent(ShapeComponent(if (Math.random() >= 0.5) "box" else "circle",
                            if (Math.random() >= 0.5) Color.web("#39c495") else Color.web("#5005C0"),
                            15.0, 15.0))
                    .addComponent(RenderableComponent(true))
        }
    }

    override fun initPlayerControls(input: Input) {
        val motion = player.getComponent(MotionComponent::class)

        input.addKeyboardAction(KeyCode.Z, object : InputAction() {

            override fun pressedDown() {
                motion.y = -SPEED_MULTIPLIER
            }

            override fun released() {
                motion.y = 0.0
            }
        }).addKeyboardAction(KeyCode.S, object : InputAction() {

            override fun pressedDown() {
                motion.y = SPEED_MULTIPLIER
            }

            override fun released() {
                motion.y = 0.0
            }
        }).addKeyboardAction(KeyCode.Q, object : InputAction() {

            override fun pressedDown() {
                motion.x = -SPEED_MULTIPLIER
            }

            override fun released() {
                motion.x = 0.0
            }
        }).addKeyboardAction(KeyCode.D, object : InputAction() {

            override fun pressedDown() {
                motion.x = SPEED_MULTIPLIER
            }

            override fun released() {
                motion.x = 0.0
            }
        })
    }

    override fun initGames() {
        for (i in 0..1000) {
            getSetting()
                    .entityFactory
                    .build(SampleType.ENEMY)
        }
    }


    override fun onUpdate(tps: Double) {
        getSetting().gameScene.getText().text = "${getSetting().gameWorld.countEntityByType(SampleType.ENEMY)}"
    }

    override fun onDraw(gc: GraphicsContext) {
    }

    override fun onExit() {
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Sample::class.java)
        }
    }
}