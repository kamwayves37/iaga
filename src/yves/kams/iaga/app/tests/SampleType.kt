package yves.kams.iaga.app.tests

/**
 * @author yves kams
 * @since 2020-06-9
 */
enum class SampleType {
    PLAYER,
    ENEMY,
    IA
}