package yves.kams.iaga.app.systems

import javafx.scene.canvas.GraphicsContext
import yves.kams.iaga.app.components.MotionComponent
import yves.kams.iaga.app.components.RenderableComponent
import yves.kams.iaga.app.components.ShapeComponent
import yves.kams.iaga.app.components.TransformComponent
import yves.kams.iaga.core.ecs.systems.System
import yves.kams.iaga.core.pools.Pool

/**
 * @author yves kams
 * @since 2020-06-15
 */
class RendererSystem : System() {
    /** */
    private lateinit var gc: GraphicsContext

    /** */
    private var canvasWidth: Double = 0.0

    /** */
    private var canvasHeight: Double = 0.0

    override fun onInit() {

        isRendered = true

        with(query) {
            keys = listOf(ShapeComponent::class, TransformComponent::class, RenderableComponent::class)
        }

        val setting = world.setting

        gc = setting.gameScene.getGraphicContext()

        canvasWidth = setting.width
        canvasHeight = setting.height
    }

    override fun onDraw(gc: GraphicsContext) {

        query.results.forEach { entity ->

            val transform = entity.getComponent(TransformComponent::class)
            val shape = entity.getComponent(ShapeComponent::class)
            val rendered = entity.getComponent(RenderableComponent::class)

            if (rendered.isRendered) {
                gc.fill = shape.color

                if (shape.primitive === "box")
                    gc.fillRect(transform.x, transform.y, shape.w, shape.h)
                else
                    gc.fillOval(transform.x, transform.y, shape.w, shape.h)
            }
        }
    }

    override fun onExit() {
        query.reset()
    }
}