package yves.kams.iaga.app.systems

import yves.kams.iaga.app.components.MotionComponent
import yves.kams.iaga.app.components.ShapeComponent
import yves.kams.iaga.app.components.TransformComponent
import yves.kams.iaga.core.ecs.systems.System

/**
 * @author yves kams
 * @since 2020-06-15
 */
class MovableSystem : System() {
    private var canvasWidth: Double = 0.0
    private var canvasHeight: Double = 0.0

    override fun onInit() {
        with(query) {
            keys = listOf(TransformComponent::class, MotionComponent::class)

            onEntityAdded = {
                //println("$it is Added")
            }

            onEntityRemoved = {
                //println("$it is Removed")
            }
        }

        val setting = world.setting

        canvasWidth = setting.width
        canvasHeight = setting.height
    }

    override fun onUpdate(tps: Double) {
        query.results.forEach { entity ->
            val motion = entity.getComponent(MotionComponent::class)
            val transform = entity.getComponent(TransformComponent::class)
            val shape = entity.getComponent(ShapeComponent::class)

            transform.x += motion.x * tps
            transform.y += motion.y * tps

            if (transform.x < -shape.w) transform.x = canvasWidth + 15.0
            if (transform.x > canvasWidth + 15.0) transform.x = -15.0
            if (transform.y < -15.0) transform.y = canvasHeight + 15.0
            if (transform.y > canvasHeight + 15.0) transform.y = -15.0
        }
    }

    override fun onExit() {
        query.reset()
    }
}