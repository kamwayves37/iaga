package yves.kams.iaga.app.systems

import yves.kams.iaga.app.components.IAComponent
import yves.kams.iaga.app.tests.SampleType
import yves.kams.iaga.core.ecs.systems.System

/**
 * @author yves kams
 * @since 2020-06-15
 */
class IASystem: System() {
    private val MAX_ELEMENTS = 1000

    override fun onInit() {
        with(query) {
            keys = listOf(IAComponent::class)

            onEntityAdded = {
                //println("$it is Added")
            }

            onEntityRemoved = {
                //println("$it is Removed")
            }
        }
    }

    override fun onUpdate(tps: Double) {
        if (Math.random() < 0.8 && world.countEntityByType(SampleType.ENEMY) < MAX_ELEMENTS) {
            world.setting.entityFactory.build(SampleType.ENEMY)
        }

        query.results.forEach { entity ->
            entity.getOptionalComponent(IAComponent::class).ifPresent { iaComponent ->
                if (iaComponent.live > 0) {
                    iaComponent.live--
                } else {
                    entity.removeToWorld()
                }
            }
        }
    }

    override fun onExit() {
    }
}