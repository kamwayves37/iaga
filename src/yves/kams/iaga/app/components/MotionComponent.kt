package yves.kams.iaga.app.components

import yves.kams.iaga.core.ecs.components.Component

/**
 * @author yves kams
 * @since 2020-06-9
 */
data class MotionComponent(var x: Double = 0.0,
                           var y: Double = 0.0) : Component()