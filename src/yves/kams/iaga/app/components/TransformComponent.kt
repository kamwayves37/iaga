package yves.kams.iaga.app.components

import yves.kams.iaga.core.ecs.components.Component

/**
 * @author yves kams
 * @since 2020-06-9
 */
data class TransformComponent(var x: Double,
                              var y: Double) : Component()