package yves.kams.iaga.app.components

import yves.kams.iaga.core.ecs.components.Component

/**
 * @author yves kams
 * @since 2020-06-15
 */
data class RenderableComponent(var isRendered: Boolean = false) : Component()