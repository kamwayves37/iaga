package yves.kams.iaga.app.components

import yves.kams.iaga.core.ecs.components.Component
import yves.kams.iaga.core.network.NeuralNetwork

/**
 * @author yves kams
 * @since 2020-06-7
 */
class NeuralComponent: Component() {
    /**
     *
     */
    val network: NeuralNetwork = NeuralNetwork()
}