package yves.kams.iaga.app.components

import javafx.scene.paint.Color
import yves.kams.iaga.core.ecs.components.Component

/**
 * @author yves kams
 * @since 2020-06-9
 */
data class ShapeComponent(val primitive: String = "box",
                          val color: Color = Color.BLACK,
                          val w: Double = 15.0,
                          val h: Double = 15.0) : Component()