package yves.kams.iaga.core.states

import javafx.scene.canvas.GraphicsContext
import yves.kams.iaga.app.states.CreditState
import yves.kams.iaga.app.states.IntroState
import yves.kams.iaga.app.states.MenuState
import yves.kams.iaga.app.states.PlayState
import yves.kams.iaga.core.concurrent.Async
import yves.kams.iaga.core.services.Service
import yves.kams.iaga.core.settings.Setting
import java.time.Duration

/**
 * @author yves kams
 * @since 2020-06-11
 */
class StateManager(setting: Setting) : Service {
    /** */
    private val gc: GraphicsContext = setting.gameScene.getGraphicContext()

    /** */
    private val gameStates: HashMap<StateType, GameState> = hashMapOf(
            StateType.INTRO to IntroState(setting.readOnlySetting),
            StateType.MENU to MenuState(setting.readOnlySetting),
            StateType.PLAY to PlayState(setting.readOnlySetting),
            StateType.CREDIT to CreditState(setting.readOnlySetting)
    )

    override fun onInit() {
        gameStates.values.forEach { it.onInit() }

        //
        Async.INSTANCE.schedule({
            CURRENT_STATE = StateType.PLAY
        }, Duration.ofMillis(1500))
    }

    override fun onUpdate(tps: Double) {
        gameStates[CURRENT_STATE]?.onUpdate(tps)
        gameStates[CURRENT_STATE]?.onDraw(gc)
    }

    override fun onExit() {
        gameStates.values.forEach { it.onExit() }
    }

    companion object {
        /**
         *
         */
        var CURRENT_STATE: StateType = StateType.INTRO
    }
}