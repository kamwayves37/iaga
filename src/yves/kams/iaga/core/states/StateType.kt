package yves.kams.iaga.core.states

/**
 * @author yves kams
 * @since 2020-06-10
 */
enum class StateType {
    INTRO,
    MENU,
    PLAY,
    CREDIT
}