package yves.kams.iaga.core.states

import javafx.scene.canvas.GraphicsContext
import yves.kams.iaga.core.settings.ReadOnlySetting

/**
 * @author yves kams
 * @since 2020-06-8
 */
abstract class GameState(setting: ReadOnlySetting) {

    /** */
    protected val screenWith: Double = setting.width

    /** */
    protected val screenHeight: Double = setting.height


    /**
     *
     */
    abstract fun onInit()

    /**
     *
     */
    abstract fun onUpdate(tps: Double)

    /**
     *
     */
    open fun onDraw(gc: GraphicsContext) {
        gc.clearRect(0.0, 0.0, screenWith, screenHeight)
    }

    /**
     *
     */
    abstract fun onExit()

}