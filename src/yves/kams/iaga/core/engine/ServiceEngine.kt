package yves.kams.iaga.core.engine

import yves.kams.iaga.core.services.Service

/**
 * @author yves kams
 * @since 2020-06-8
 */
class ServiceEngine: Service {

    /**
     *
     */
    override fun onInit() {
        //TODO("Not yet implemented")
    }

    /**
     * @param tps
     */
    override fun onUpdate(tps: Double) {
        //TODO("Not yet implemented")
    }

    /**
     *
     */
    override fun onExit() {
        //TODO("Not yet implemented")
    }
}