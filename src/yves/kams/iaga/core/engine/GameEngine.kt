package yves.kams.iaga.core.engine

import yves.kams.iaga.core.games.GameApp
import yves.kams.iaga.core.inputs.Input
import yves.kams.iaga.core.services.Service
import yves.kams.iaga.core.settings.Setting
import yves.kams.iaga.core.states.StateManager

/**
 * @author yves kams
 * @since 2020-06-15
 */
class GameEngine(setting: Setting) {

    /** */
    private val loop = GameLoop(this::update)

    /** */
    private val services: List<Service> = listOf(
            Input(setting),
            StateManager(setting),
            GameApp.GameService(setting.gameApp),
            ServiceEngine()
    )

    /**
     * Main loop of the game
     * which allows to refresh the game
     * and update the rendering
     * @param tps
     */
    private fun update(tps: Double) {
        services.forEach { it.onUpdate(tps) }
    }

    /**
     * Starts the game
     * and initializes the services
     * */
    fun start() {
        services.forEach { it.onInit() }

        loop.start()
    }

    /** Pauses the game */
    fun pause() {
        loop.pause()
    }

    /** Resumes play in pause */
    fun resume() {
        loop.resume()
    }

    /**
     * Stops the game
     * and closes the services
     */
    fun stop() {
        loop.stop()
        services.forEach { it.onExit() }
    }
}