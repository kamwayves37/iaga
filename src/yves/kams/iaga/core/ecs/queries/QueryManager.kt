package yves.kams.iaga.core.ecs.queries

import javafx.event.EventHandler
import yves.kams.iaga.core.ecs.components.ComponentEvent
import yves.kams.iaga.core.ecs.entities.EntityEvent
import yves.kams.iaga.core.event.EventBus
import yves.kams.iaga.core.event.Subscribe

/**
 * @author yves kams
 * @since 2020-06-12
 */
class QueryManager {
    /** */
    private val queries: MutableList<Query> = mutableListOf()

    /** */
    private var subscribeEntity: Subscribe

    /** */
    private var subscribeComponent: Subscribe

    init {
        //
        subscribeEntity = EventBus.INSTANCE.addEventHandler(EntityEvent.ENTITY_REMOVED, EventHandler { onEntityRemoved(it) })

        //
        subscribeComponent = EventBus.INSTANCE.addEventHandler(ComponentEvent.ANY, EventHandler {
            when (it.eventType) {
                ComponentEvent.COMPONENT_ADDED -> onComponentAdded(it)
                ComponentEvent.COMPONENT_REMOVED -> onComponentRemoved(it)
            }
        })
    }


    /**
     * @param query
     */
    fun register(query: Query) {
        queries.add(query)
    }

    /**
     * @param event
     */
    private fun onEntityRemoved(event: EntityEvent) {
        queries.forEach {
            if (it.hasEntity(event.entity) && !it.hasEntityRemoved(event.entity))
                it.removeEntity(event.entity)
        }
    }

    /**
     * @param event
     */
    private fun onComponentAdded(event: ComponentEvent) {
        queries.forEach {
            if (it.match(event.entity.typesComponents()) &&
                    !it.hasEntity(event.entity) &&
                    !it.hasEntityAdded(event.entity)) {
                if (it.match(event.entity.typesComponents())) {
                    println()
                }
                it.addEntity(event.entity)

            }
        }
    }

    /**
     * @param event
     */
    private fun onComponentRemoved(event: ComponentEvent) {
        queries.forEach {
            if (it.match(event.component::class) &&
                    it.hasEntity(event.entity) &&
                    !it.hasEntityRemoved(event.entity))
                it.removeEntity(event.entity)
        }
    }

    /**
     *
     */
    fun destroy() {
        queries.forEach { it.reset() }
        subscribeEntity.unSubscribe()
        subscribeComponent.unSubscribe()
    }
}