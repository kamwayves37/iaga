package yves.kams.iaga.core.ecs.queries

import yves.kams.iaga.core.ecs.components.Component
import yves.kams.iaga.core.ecs.entities.Entity
import kotlin.reflect.KClass

/**
 * @author yves kams
 * @since 2020-06-12
 */
class Query {

    /** */
    var keys: List<KClass<out Component>> = listOf()

    /** */
    var onEntityAdded: ((Entity) -> Any)? = null

    /** */
    var onEntityRemoved: ((Entity) -> Any)? = null

    /** */
    private val entities: MutableList<Entity> = mutableListOf()

    /** */
    private val entitiesAdded: MutableList<Entity> = mutableListOf()

    /** */
    private val entitiesRemoved: MutableList<Entity> = mutableListOf()

    /** */
    private var onAdded = false

    /** */
    private var onRemoved = false

    /** */
    val results: List<Entity>
        get() {
            if (onAdded) {
                entities.addAll(entitiesAdded)
                entitiesAdded.clear()
                onAdded = false
            }

            if (onRemoved) {
                entities.removeAll(entitiesRemoved)
                entitiesRemoved.clear()
                onRemoved = false
            }

            return entities
        }

    /**
     * @param type
     * @return boolean
     */
    fun match(type: Set<KClass<out Component>>): Boolean {
        return type.containsAll(keys) && keys.isNotEmpty()
    }

    /**
     * @param type
     * @return boolean
     */
    fun match(type: KClass<out Component>): Boolean {
        return keys.contains(type) && keys.isNotEmpty()
    }

    /**
     * @param entity
     * @return boolean
     */
    fun hasEntity(entity: Entity): Boolean {
        return entities.contains(entity)
    }

    /**
     * @param entity
     * @return boolean
     */
    fun hasEntityAdded(entity: Entity): Boolean {
        return entitiesAdded.contains(entity)
    }

    /**
     * @param entity
     * @return boolean
     */
    fun hasEntityRemoved(entity: Entity): Boolean {
        return entitiesRemoved.contains(entity)
    }

    /**
     * @param entity
     */
    fun removeEntity(entity: Entity) {
        entitiesRemoved.add(entity)
        onEntityRemoved?.invoke(entity)
        onRemoved = true
    }

    /**
     * @param entity
     */
    fun addEntity(entity: Entity) {
        entitiesAdded.add(entity)
        onEntityAdded?.invoke(entity)
        onAdded = true
    }

    /**
     *
     */
    fun reset() {
        onRemoved = false
        onAdded = false
        entities.clear()
        entitiesRemoved.clear()
        entitiesAdded.clear()
    }
}
