package yves.kams.iaga.core.ecs.components

import kotlin.reflect.KClass

/**
 * @author yves kams
 * @since 2020-06-11
 */
class ComponentManager {

    /** */
    private val components: MutableMap<KClass<out Component>, Component> = mutableMapOf()

    /** */
    val size: Int = components.size

    /**
     * @param component
     *
     */
    fun add(component: Component) {
        components[component::class] = component
    }

    /**
     * @param type
     * @return Component
     */
    fun <T : Component> get(type: KClass<T>): Component? {
        return components[type]
    }

    /**
     * @param type
     * @return boolean
     */
    fun <T : Component> remove(type: KClass<T>): Component? {
        return components.remove(type)
    }

    /**
     *
     */
    fun removeAll() {
        components.clear()
    }

    /**
     * @param type
     * @return boolean
     */
    fun <T : Component> has(type: KClass<T>): Boolean {
        return components.containsKey(type)
    }

    /**
     * @return MutableSet
     */
    fun types(): MutableSet<KClass<out Component>> {
        return components.keys
    }
}