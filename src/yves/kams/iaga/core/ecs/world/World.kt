package yves.kams.iaga.core.ecs.world

import javafx.scene.canvas.GraphicsContext
import yves.kams.iaga.core.ecs.entities.Entity
import yves.kams.iaga.core.ecs.entities.EntityManager
import yves.kams.iaga.core.ecs.systems.System
import yves.kams.iaga.core.ecs.systems.SystemManager
import yves.kams.iaga.core.services.Service
import yves.kams.iaga.core.settings.ReadOnlySetting
import yves.kams.iaga.core.settings.Setting
import kotlin.reflect.KClass

/**
 * @author yves kams
 * @since 2020-06-15
 */
class World(val setting: ReadOnlySetting) : Service {

    /** */
    private val systemManager = SystemManager(this)

    /** */
    private val entityManager = EntityManager(this)

    /**
     * Searches entities by type
     * @param type type of entity to look for
     * @return List<Entity> The filter result
     */
    fun entityByTypes(type: Enum<*>): List<Entity> {
        TODO("Not yet implemented")
    }

    /**
     * @param type
     * @return int
     */
    fun countEntityByType(type: Enum<*>): Int {
        return entityManager.countByType(type)
    }

    /**
     * Create a new entity instance
     * @param type entity type
     * @return entity
     */
    fun createEntity(type: Enum<*>): Entity {
        return entityManager.create(type)
    }

    /**
     * @param entity
     * @return boolean
     */
    fun destroyEntity(entity: Entity): Boolean {
        TODO("Not yet implemented")
    }

    /**
     * @param type
     * @return world
     */
    fun <T : System> registerSystem(type: KClass<T>): World {
        systemManager.register(type)
        return this
    }

    /**
     * @param type
     * @return boolean
     */
    fun <T : System> hasSystem(type: KClass<T>): Boolean {
        return systemManager.has(type)
    }

    /**
     * @param type
     */
    fun <T : System> unRegisterSystem(type: KClass<T>) {
        TODO("Not yet implemented")
    }

    /** Initialize all systems */
    override fun onInit() {
        systemManager.onInit()
    }

    /**
     * updates all systems
     * @param tps
     */
    override fun onUpdate(tps: Double) {
        systemManager.onUpdate(tps)
    }

    fun onDraw(gc: GraphicsContext) {
        systemManager.onDraw(gc)
    }

    /** exit all systems */
    override fun onExit() {
        systemManager.onExit()
    }
}