package yves.kams.iaga.core.ecs.systems

import javafx.scene.canvas.GraphicsContext
import yves.kams.iaga.core.ecs.queries.Query
import yves.kams.iaga.core.ecs.world.World

/**
 * @author yves kams
 * @since 2020-06-12
 */
abstract class System {
    /** */
    val query: Query = Query()

    /** */
    lateinit var world: World

    var isRendered: Boolean = false

    /**
     *
     */
    open fun onInit() {}

    /**
     * @param tps
     */
    open fun onUpdate(tps: Double) {}

    open fun onDraw(gc: GraphicsContext) {}

    /**
     *
     */
    open fun onExit() {}
}