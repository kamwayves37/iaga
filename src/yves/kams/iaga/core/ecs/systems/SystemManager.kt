package yves.kams.iaga.core.ecs.systems

import javafx.scene.canvas.GraphicsContext
import yves.kams.iaga.core.ecs.queries.QueryManager
import yves.kams.iaga.core.ecs.world.World
import yves.kams.iaga.core.utils.reflection.Reflection
import kotlin.reflect.KClass

/**
 * @author yves kams
 * @since 2020-06-12
 */
class SystemManager(private val world: World) {

    /** */
    private val systems: MutableList<System> = mutableListOf()

    /** */
    private val cacheSystems: MutableMap<KClass<out System>, System?> = mutableMapOf()

    /** */
    private val queryManager: QueryManager = QueryManager()

    /**
     * @param type
     */
    fun <T : System> register(type: KClass<T>) {
        cacheSystems[type] = null
    }

    /**
     * @param type
     */
    fun <T : System> destroy(type: KClass<T>): Boolean {
        TODO("Not yet implemented")
    }

    /**
     * @param type
     * @return boolean
     */
    fun <T : System> has(type: KClass<T>): Boolean {
        return cacheSystems.containsKey(type)
    }

    /**
     * @return sashSet<>
     */
    fun types(): Set<KClass<out System>> {
        return cacheSystems.keys
    }

    /**
     */
    fun onInit() {
        for (type in types()) {
            val system = Reflection.newInstance(type.java)
            system.world = world

            system.onInit()

            queryManager.register(system.query)

            cacheSystems[type] = system
            systems.add(system)
        }
    }

    /**
     * @param tps
     */
    fun onUpdate(tps: Double) {
        systems.forEach { it.onUpdate(tps) }
    }

    /**
     * @param gc
     */
    fun onDraw(gc: GraphicsContext) {
        systems.filter {
            it.isRendered
        }.map {
            it.onDraw(gc)
        }
    }

    /**
     *
     */
    fun onExit() {
        systems.forEach { it.onExit() }
    }
}