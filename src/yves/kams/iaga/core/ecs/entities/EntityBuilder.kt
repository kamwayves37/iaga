package yves.kams.iaga.core.ecs.entities

import yves.kams.iaga.core.ecs.components.Component

/**
 * @author yves kams
 * @since 2020-06-9
 */
class EntityBuilder {

    fun type(type: Enum<*>): EntityBuilder {
        TODO("Not yet implemented")
    }

    fun assign(component: Component): EntityBuilder {
        TODO("Not yet implemented")
    }

    fun build(): Entity {
        TODO("Not yet implemented")
    }
}