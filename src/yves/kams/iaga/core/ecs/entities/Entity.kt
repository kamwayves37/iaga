package yves.kams.iaga.core.ecs.entities

import yves.kams.iaga.core.ecs.components.Component
import yves.kams.iaga.core.ecs.world.World
import yves.kams.iaga.core.utils.generator.IDGenerator
import java.util.*
import kotlin.collections.HashSet
import kotlin.reflect.KClass

/**
 * @author yves kams
 * @since 2020-06-11
 */
class Entity(private val manager: EntityManager) {

    /**  */
    lateinit var type: Enum<*>

    /** */
    lateinit var world: World

    /** */
    val id: Int = IDGenerator.generateID()

    /**
     *
     */
    fun removeToWorld() {
        manager.destroy(this)
    }

    /**
     * @param component
     * @return entity
     */
    fun <T : Component> addComponent(component: T): Entity {
        manager[this] = component
        return this
    }

    /**
     * @param type
     * @return T
     */
    fun <T : Component> getComponent(type: KClass<T>): T {
        return manager[this, type]
    }

    /**
     * @param type
     * @return T
     */
    fun <T : Component> getOptionalComponent(type: KClass<T>): Optional<T> {
        return Optional.ofNullable(manager[this, type])
    }

    /**
     * @param type
     */
    fun <T : Component> removeComponent(type: KClass<T>): Boolean {
        return manager.remove(this, type)
    }

    /**
     *
     */
    fun removeAllComponents() {
        manager.removeAll(this)
    }

    /**
     * @param type
     */
    fun <T : Component> hasComponent(type: KClass<T>): Boolean {
        return manager.has(this, type)
    }

    /**
     * @return
     */
    fun typesComponents(): Set<KClass<out Component>> {
        return manager.types(this)
    }

    /**
     * @return string
     */
    override fun toString(): String {
        return "Entity{Id: ".plus(id).plus(", Type: ").plus(type.name).plus("}")
    }
}