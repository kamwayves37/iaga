package yves.kams.iaga.core.ecs.entities

import yves.kams.iaga.core.ecs.components.Component
import yves.kams.iaga.core.ecs.components.ComponentEvent
import yves.kams.iaga.core.ecs.components.ComponentManager
import yves.kams.iaga.core.ecs.world.World
import yves.kams.iaga.core.event.EventBus
import kotlin.reflect.KClass
import kotlin.reflect.cast

/**
 * @author yves kams
 * @since 2020-06-15
 */
class EntityManager(private val world: World) {

    /** */
    private val entities: MutableMap<Entity, ComponentManager> = mutableMapOf()

    /**
     * @param type
     * @return entity
     */
    fun create(type: Enum<*>): Entity {
        val entity = Entity(this)

        entity.type = type
        entity.world = world

        entities[entity] = ComponentManager()

        EventBus.INSTANCE.fireEvent(EntityEvent(EntityEvent.ENTITY_CREATED, entity))

        return entity
    }

    /**
     * @param entity
     */
    fun destroy(entity: Entity) {
        val manager = entities.remove(entity)
        manager?.removeAll()

        EventBus.INSTANCE.fireEvent(EntityEvent(EntityEvent.ENTITY_REMOVED, entity))
    }

    /**
     * @param type
     * @return list<Entity>
     */
    fun filterByType(type: Enum<*>): List<Entity> {
        return entities.keys
                .filter { entity -> entity.type === type }
                .toList()
    }

    /**
     * @param type
     * @return int
     */
    fun countByType(type: Enum<*>): Int {
        return entities.keys
                .filter { entity -> entity.type === type }
                .size
    }

    /**
     * @param entity
     * @param component
     */
    operator fun set(entity: Entity, component: Component) {
        entities[entity]?.add(component)

        EventBus.INSTANCE.fireEvent(ComponentEvent(ComponentEvent.COMPONENT_ADDED, entity, component))
    }

    /**
     * @param entity
     * @param type
     */
    operator fun <T : Component> get(entity: Entity, type: KClass<T>): T {
        val component = entities[entity]?.get(type)

        if (component !== null)
            return type.cast(component)

        throw IllegalAccessError("Error accessing component " + type.simpleName + " in $entity")
    }

    fun <T : Component> has(entity: Entity, type: KClass<T>): Boolean {
        return entities[entity]?.has(type) ?: false
    }

    fun <T : Component> remove(entity: Entity, type: KClass<T>): Boolean {
        val component = entities[entity]?.remove(type)

        if (component !== null)
            EventBus.INSTANCE.fireEvent(ComponentEvent(ComponentEvent.COMPONENT_REMOVED, entity, component))

        return component !== null
    }

    fun removeAll(entity: Entity) {
        entities[entity]?.removeAll()
    }

    fun types(entity: Entity): Set<KClass<out Component>> {
        return entities[entity]?.types() ?: throw IllegalAccessError("Error accessing types component in $entity")
    }
}