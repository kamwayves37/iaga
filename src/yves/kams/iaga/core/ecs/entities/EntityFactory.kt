package yves.kams.iaga.core.ecs.entities


/**
 * @author yves kams
 * @since 2020-06-15
 */
class EntityFactory {
    private val factories: MutableMap<Enum<*>, () -> Entity> = mutableMapOf()

    operator fun set(type: Enum<*>, model: () -> Entity) {
        factories[type] = model
    }

    operator fun get(type: Enum<*>): Entity {
        val model = factories[type]

        if (model !== null)
            return model.invoke()

        throw NullPointerException("the type $type does not exist")
    }

    fun build(type: Enum<*>) {
        factories[type]?.invoke()
    }
}