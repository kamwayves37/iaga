package yves.kams.iaga.core.ecs.entities

import javafx.event.Event
import javafx.event.EventType

/**
 * @author yves kams
 * @since 2020-06-11
 */
class EntityEvent(eventType: EventType<EntityEvent>, val entity: Entity) : Event(eventType) {
    companion object {
        /** */
        val ANY = EventType<EntityEvent>(Event.ANY, "ENTITY EVENT")

        /** */
        val ENTITY_CREATED = EventType(ANY, "ENTITY CREATED")

        /** */
        val ENTITY_REMOVED = EventType(ANY, "ENTITY REMOVED")
    }
}