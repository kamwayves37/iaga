package yves.kams.iaga.core.concurrent

import javafx.concurrent.Task
import java.util.function.Function

/**
 * @author yves kams
 * @since 2020-06-9
 */
abstract class IOTask<T> {
    private val name: String
    private var hasFailAction: Boolean = false
    private var successAction: (T) -> Unit = {}
    private var failAction: (Throwable) -> Unit = { println(it.message) }

    val isHasFailAction: Boolean
        get() = hasFailAction

    constructor(name: String) {
        this.name = name
    }

    constructor() : this(DEFAULT_NAME)

    fun onSuccess(successAction: (T) -> Unit): IOTask<T> {
        this.successAction = successAction
        return this
    }

    fun onFailure(failAction: (Throwable) -> Unit): IOTask<T> {
        this.failAction = failAction
        this.hasFailAction = true
        return this
    }

    fun run(): T? {
        return try {
            val value: T = onExecute()
            succeed(value)
            value
        } catch (e: Exception) {
            fail(e)
            null
        }
    }

    fun <R> then(mapper: Function<T, IOTask<R>>): IOTask<R> {
        return of(this.name) { mapper.apply(onExecute()).onExecute() }
    }

    fun <R> thenWrap(mapper: Function<T, R>): IOTask<R> {
        return then(Function { t -> of { mapper.apply(t) } })
    }

    fun toJavaFXTask(): Task<T> {
        return object : Task<T>() {
            override fun call(): T {
                return onExecute()
            }

            override fun succeeded() {
                succeed(value)
            }

            override fun failed() {
                fail(exception)
            }
        }
    }

    @Throws(Exception::class)
    protected abstract fun onExecute(): T

    private fun succeed(value: T) {
        this.successAction.invoke(value)
    }

    private fun fail(error: Throwable) {
        this.failAction.invoke(error)
    }

    companion object {

        private const val DEFAULT_NAME: String = "NoName"

        fun ofVoid(runnable: () -> Unit): IOTask<Unit> {
            return ofVoid(DEFAULT_NAME, runnable)
        }

        fun ofVoid(name: String, runnable: () -> Unit): IOTask<Unit> {
            return of(name) {
                runnable.invoke()
            }
        }

        fun <R> of(name: String, action: () -> R): IOTask<R> {
            return object : IOTask<R>(name) {
                @Throws(Exception::class)
                override fun onExecute(): R {
                    return action.invoke()
                }

            }
        }

        fun <R> of(action: () -> R): IOTask<R> {
            return of(DEFAULT_NAME, action)
        }
    }
}