package yves.kams.iaga.core.concurrent

import java.time.Duration
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * @author yves kams
 * @since 2020-06-9
 */
enum class Async : Executor {
    INSTANCE;

    private val service = Executors.newCachedThreadPool(FXThreadFactory())
    private val scheduledService = Executors.newScheduledThreadPool(2, FXThreadFactory())

    override fun execute(runnable: Runnable) {
        service.execute(runnable)
    }

    fun schedule(action: () -> Unit, delay: Duration): ScheduledFuture<*> {
        return scheduledService.schedule(action, delay.toMillis(), TimeUnit.MILLISECONDS)
    }

    fun shutdownNow() {
        try {
            println("attempt to shutdown executor")
            service.shutdown()
            scheduledService.shutdown()
            service.awaitTermination(5, TimeUnit.SECONDS)
        } catch (e: InterruptedException) {
            System.err.println("tasks interrupted")
        } finally {
            if (!service.isTerminated || !scheduledService.isTerminated) {
                println("cancel non-finished tasks")
            }
            println("shutdown finished")
        }
    }

    private class FXThreadFactory : ThreadFactory {
        private val threadNumber = AtomicInteger(0)
        override fun newThread(runnable: Runnable): Thread {
            val thread = Thread(runnable, "FX Background Thread " + threadNumber.getAndIncrement())
            thread.isDaemon = false
            thread.priority = Thread.NORM_PRIORITY
            return thread
        }
    }
}