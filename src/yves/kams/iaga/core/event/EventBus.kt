package yves.kams.iaga.core.event

import javafx.event.Event
import javafx.event.EventHandler
import javafx.event.EventType
import javafx.scene.Group

/**
 * @author yves kams
 * @since 2020-06-11
 */
enum class EventBus {
    INSTANCE;

    private val group: Group = Group()

    /**
     * @param event
     */
    fun fireEvent(event: Event) {
        group.fireEvent(event)
    }

    /**
     * @param type
     * @param handler
     */
    fun <T : Event> addEventHandler(type: EventType<out T>, handler: EventHandler<in T>): Subscribe {
        group.addEventHandler(type, handler)

        @Suppress("UNCHECKED_CAST")
        return Subscribe(type, handler as EventHandler<in Event>)
    }

    /**
     * @param type
     * @param handler
     */
    fun <T : Event> removeEventHandler(type: EventType<out T>, handler: EventHandler<in T>) {
        group.removeEventHandler(type, handler)
    }
}