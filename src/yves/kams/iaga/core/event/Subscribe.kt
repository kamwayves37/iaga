package yves.kams.iaga.core.event

import javafx.event.Event
import javafx.event.EventHandler
import javafx.event.EventType

/**
 * @author yves kams
 * @since 2020-06-11
 */
class Subscribe(private val type: EventType<out Event>,
                private val handler: EventHandler<in Event>) {

    /**
     *
     */
    fun unSubscribe() {
        EventBus.INSTANCE.removeEventHandler(type, handler)
    }
}