package yves.kams.iaga.core.network

import yves.kams.iaga.core.utils.math.Matrix

/**
 * @author yves kams
 * @since 2020-06-7
 */
class Layer {

    /**
     * @param other
     */
    private constructor(other: Layer) {
        TODO("Not implement")
    }

    /**
     * @return layer
     */
    fun copy(): Layer {
        TODO("Not implement")
    }

    /**
     * @return layer
     */
    fun newInstance(): Layer {
        TODO("Not implement")
    }

    /**
     * @return int
     */
    fun getINodes(): Int {
        TODO("Not implement")
    }

    /**
     * @return int
     */
    fun getONodes(): Int {
        TODO("Not implement")
    }

    /**
     * @param inputs
     */
    fun forward(inputs: Matrix){
        TODO("Not implement")
    }

    /**
     * @param inputs
     */
    fun backward(dout: Matrix, outputPrev: Matrix, learningRate: Float){
        TODO("Not implement")
    }

    /**
     *
     */
    fun mutate(mutationRate: Float){
        TODO("Not implement")
    }

    /**
     *
     */
    fun crossover(): Layer {
        TODO("Not implement")
    }
}