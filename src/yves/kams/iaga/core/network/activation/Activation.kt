package yves.kams.iaga.core.network.activation

/**
 * @author yves kams
 * @since 2020-06-7
 */
class Activation {
    companion object {
        /**
         *
         */
        var SIGMOID_GAIN: Float = 1F

        /**
         * Sigmoid function
         */
        val SIGMOID: IActivation = object : IActivation {
            override fun func(x: Float) {
                TODO("Not yet implemented")
            }

            override fun dfunc(y: Float) {
                TODO("Not yet implemented")
            }
        }

        /**
         * ReLu function
         */
        val ReLU: IActivation = object : IActivation {
            override fun func(x: Float) {
                TODO("Not yet implemented")
            }

            override fun dfunc(y: Float) {
                TODO("Not yet implemented")
            }
        }

        /**
         * TanH function
         */
        val TanH: IActivation = object : IActivation {
            override fun func(x: Float) {
                TODO("Not yet implemented")
            }

            override fun dfunc(y: Float) {
                TODO("Not yet implemented")
            }
        }
    }
}