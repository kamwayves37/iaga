package yves.kams.iaga.core.network.activation

/**
 * @author yves kams
 * @since 2020-06-8
 */
interface IActivation {

    /**
     * Activation feature
     * @param x value a activated
     */
    fun func(x: Float)

    /**
     * Derived from the activation function
     * @param y value a derived
     */
    fun dfunc(y: Float)
}