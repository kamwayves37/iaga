package yves.kams.iaga.core.network

/**
 * @author yves kams
 * @since 2020-06-8
 */
class NeuralNetwork() {

    /**
     * Copy builder
     * @param other copy neuralNetwork
     */
    private constructor(other: NeuralNetwork): this() {
        TODO("Not implement")
    }

    /**
     * A copy of the current neural network
     * @return neuralNetwork The new instance of the current network
     */
    fun copy(): NeuralNetwork {
        TODO("Not implement")
    }

    /**
     * A new instance of the current neural network
     * with the same number of inputs and outputs.
     * @return neuralNetwork
     */
    fun newInstance(): NeuralNetwork {
        TODO("Not implement")
    }

    /**
     * @param inputArray data to be predicted
     * @return float[]
     */
    fun predict(inputArray: FloatArray): FloatArray {
        TODO("Not implement")
    }

    /**
     * Train the neural network
     * Propagate it both forward and backward
     * @param inputArray training data
     * @param targetArray expected value
     */
    fun train(inputArray: FloatArray, targetArray: FloatArray) {
        TODO("Not implement")
    }

    /**
     * Performs a mutation of each layer
     * of the neural network
     */
    fun mutate() {
        TODO("Not implement")
    }

    /**
     * Crosses two neural networks that are parents
     * and give birth to a child's neural network.
     * @param other Is the second parent
     * @return neuralNetwork The result of the crossing
     */
    fun crossover(other: NeuralNetwork): NeuralNetwork {
        TODO("Not implement")
    }

    companion object {
        /**
         * Learning rate which allows to define
         * the speed at which the network learns.
         */
        var learningRate: Float = 0.01F

        /**
         * Mutation rate which allows to define
         * the speed at which the network mutes
         * the neurons of each layer.
         */
        var mutationRate: Float = 0.01F
    }
}