package yves.kams.iaga.core.inputs

/**
 * @author yves kams
 * @since 2020-06-10
 */
data class InputState(var prev: Boolean = false, var current: Boolean = false)