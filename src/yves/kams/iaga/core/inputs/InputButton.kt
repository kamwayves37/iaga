package yves.kams.iaga.core.inputs

/**
 * @author yves kams
 * @since 2020-06-10
 */
data class InputButton(var pressed: Boolean = false,
                       var down: Boolean = false,
                       var up: Boolean = false) {
    /**
     *
     */
    fun reset() {
        pressed = false
        down = false
        up = false
    }
}