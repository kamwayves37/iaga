package yves.kams.iaga.core.inputs

import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import yves.kams.iaga.core.services.Service
import yves.kams.iaga.core.settings.Setting

/**
 * @author yves kams
 * @since 2020-06-12
 */
class Input(private val setting: Setting) : InputActionListener, KeyboardState, MouseState, Service {

    /** */
    private val actions: MutableMap<Enum<*>, InputAction> = mutableMapOf()

    /** */
    private val mapping: MutableMap<Enum<*>, InputState> = mutableMapOf()

    /** */
    private val currentAction: MutableMap<Enum<*>, Boolean> = mutableMapOf()

    /** */
    private val currentCache: MutableList<Enum<*>> = mutableListOf()

    /** */
    private val states: MutableMap<Enum<*>, InputButton> = mutableMapOf()

    /** */
    private var sceneX: Double = 0.0

    /** */
    private var sceneY: Double = 0.0

    /** */
    override val mouseX: Double
        get() = sceneX

    /** */
    override val mouseY: Double
        get() = sceneY

    init {
        //
        setting.input = this
    }

    override fun addKeyboardAction(key: KeyCode, action: InputAction): InputActionListener {
        actions[key] = action
        return this
    }

    override fun addMouseAction(key: MouseButton, action: InputAction): InputActionListener {
        actions[key] = action
        return this
    }

    override fun onInit() {
        //
        setting.gameScene
                .getScene()
                .addEventFilter(KeyEvent.ANY) { event ->
                    when (event.eventType) {
                        KeyEvent.KEY_PRESSED -> {
                            setKey(event.code, true)
                            currentAction[event.code] = true
                        }

                        KeyEvent.KEY_RELEASED -> {
                            setKey(event.code, false)
                        }
                    }
                }

        //
        setting.gameScene
                .getScene()
                .addEventFilter(MouseEvent.ANY) { event ->

                    when (event.eventType) {
                        MouseEvent.MOUSE_PRESSED -> {
                            setKey(event.button, true)
                            currentAction[event.button] = true
                        }

                        MouseEvent.MOUSE_RELEASED -> {
                            setKey(event.button, false)
                        }

                        MouseEvent.MOUSE_MOVED -> {
                            sceneX = event.sceneX
                            sceneY = event.sceneY
                        }

                        MouseEvent.MOUSE_DRAGGED -> {
                            sceneX = event.sceneX
                            sceneY = event.sceneY
                        }
                    }
                }

        //Init Default input action
    }

    override fun onUpdate(tps: Double) {
        currentAction.forEach { (key, status) ->
            val button = getButton(key)

            button.reset()

            if (status) {
                val state = getKey(key)
                val action = actions[key]

                if (!state.prev && state.current) {
                    action?.onPressed()
                    button.down = true
                }

                if (state.prev && state.current) {
                    action?.onPressedDown()
                    button.pressed = true
                }

                if (!state.current && state.prev) {
                    action?.onReleased()
                    currentAction[key] = false
                    currentCache.add(key)
                    button.up = true
                }

                state.prev = state.current

            }
        }

        //
        if (currentCache.isNotEmpty()) {
            currentCache.forEach { key ->
                currentAction.remove(key)
            }
            currentCache.clear()
        }
    }

    /**
     * @param key
     * @return InputButton
     */
    private fun getButton(key: Enum<*>): InputButton {
        var inputButton = states[key]

        if (inputButton === null) {
            inputButton = InputButton()
            states[key] = inputButton
        }

        return inputButton
    }

    /**
     * @param key
     * @return InputState
     */
    private fun getKey(key: Enum<*>): InputState {
        var state = mapping[key]

        if (state === null) {
            state = InputState()
            mapping[key] = state
        }

        return state
    }

    /**
     * @param key
     * @param status
     */
    private fun setKey(key: Enum<*>, status: Boolean) {
        val state = getKey(key)
        state.prev = state.current
        state.current = status
    }

    override fun isKeyPressed(key: KeyCode): Boolean {
        return states[key]?.pressed ?: false
    }

    override fun isKeyDown(key: KeyCode): Boolean {
        return states[key]?.down ?: false
    }

    override fun isKeyUp(key: KeyCode): Boolean {
        return states[key]?.up ?: false
    }

    override fun mouseDown(button: MouseButton): Boolean {
        return states[button]?.down ?: false
    }

    override fun mouseClicked(button: MouseButton): Boolean {
        return states[button]?.pressed ?: false
    }

    override fun mouseUp(button: MouseButton): Boolean {
        return states[button]?.up ?: false
    }

    override fun onExit() {
        currentAction.clear()
        currentCache.clear()
        actions.clear()
        mapping.clear()
        states.clear()
    }
}