package yves.kams.iaga.core.inputs

import javafx.scene.input.KeyCode
import javafx.scene.input.MouseButton

/**
 * @author yves kams
 * @since 2020-06-10
 */
interface InputActionListener {
    /**
     *
     */
    fun addKeyboardAction(key: KeyCode, action: InputAction): InputActionListener

    /**
     *
     */
    fun addMouseAction(key: MouseButton, action: InputAction): InputActionListener
}