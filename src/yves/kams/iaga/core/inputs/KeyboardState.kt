package yves.kams.iaga.core.inputs

import javafx.scene.input.KeyCode

/**
 * @author yves kams
 * @since 2020-06-12
 */
interface KeyboardState {
    /**
     * check if the key is pressed
     * @param key checked
     */
    fun isKeyDown(key: KeyCode): Boolean = false

    /**
     * check if the key is released
     * @param key checked
     */
    fun isKeyUp(key: KeyCode): Boolean = false

    /**
     * check if the key has just been pressed
     * @param key checked
     */
    fun isKeyPressed(key: KeyCode): Boolean = false
}