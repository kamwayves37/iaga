package yves.kams.iaga.core.inputs

import javafx.scene.input.MouseButton

/**
 * @author yves kams
 * @since 2020-06-12
 */
interface MouseState {
    /** */
    val mouseX: Double

    /** */
    val mouseY: Double

    /**
     * @param button
     * @return boolean
     */
    fun mouseDown(button: MouseButton): Boolean = false

    /**
     * @param button
     * @return boolean
     */
    fun mouseClicked(button: MouseButton): Boolean = false

    /**
     * @param button
     * @return boolean
     */
    fun mouseUp(button: MouseButton): Boolean = false
}