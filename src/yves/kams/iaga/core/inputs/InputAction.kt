package yves.kams.iaga.core.inputs

/**
 * @author yves kams
 * @since 2020-06-10
 */
abstract class InputAction {

    /** Execute the action pressed */
    fun onPressed() {
        pressed()
    }

    /** Execute the action pressedDown */
    fun onPressedDown() {
        pressedDown()
    }

    /** Execute the action released */
    fun onReleased() {
        released()
    }

    /** Action when the user presses a key on the keyboard or mouse */
    protected open fun pressed() {}

    /** Action when the user holds down a key on the keyboard  or mouse */
    protected open fun pressedDown() {}

    /** action when the user releases a key on the keyboard  or mouse */
    protected open fun released() {}
}