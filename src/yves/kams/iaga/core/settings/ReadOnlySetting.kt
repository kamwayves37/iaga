package yves.kams.iaga.core.settings

import yves.kams.iaga.core.ecs.entities.EntityFactory
import yves.kams.iaga.core.ecs.world.World
import yves.kams.iaga.core.games.GameApp
import yves.kams.iaga.core.games.GameScene
import yves.kams.iaga.core.inputs.KeyboardState
import yves.kams.iaga.core.inputs.MouseState

/**
 * @author yves kams
 * @since 2020-06-12
 */
class ReadOnlySetting(private val setting: Setting) {

    /** Get the width of the game scene */
    val width: Double
        get() = setting.width

    /** Get the height of the game scene */
    val height: Double
        get() = setting.height

    /** get the width of the game scene */
    val title: String
        get() = setting.title

    /** Get window title */
    val version: String
        get() = setting.version

    /** Get game version */
    val resizable: Boolean
        get() = setting.resizable

    /** */
    val debug: Boolean
        get() = setting.debug

    /** */
    val gameScene: GameScene
        get() = setting.gameScene

    /** */
    val gameApp: GameApp
        get() = setting.gameApp

    /** */
    val gameWorld: World
        get() = setting.gameWorld

    /** */
    val keyboardState: KeyboardState
        get() = setting.input

    /** */
    val mouseState: MouseState
        get() = setting.input

    /** */
    val entityFactory: EntityFactory
        get() = setting.entityFactory
}