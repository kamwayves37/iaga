package yves.kams.iaga.core.settings

import yves.kams.iaga.core.ecs.entities.EntityFactory
import yves.kams.iaga.core.ecs.world.World
import yves.kams.iaga.core.games.GameApp
import yves.kams.iaga.core.games.GameScene
import yves.kams.iaga.core.inputs.Input

/**
 * @author yves kams
 * @since 2020-06-10
 */
class Setting {

    /** Define the width of the game scene */
    var width: Double = 600.0

    /** Define the height of the game scene */
    var height: Double = 400.0

    /** Gives a title to the game window */
    var title: String = "IAGA"

    /** Gives the version of the game */
    var version: String = "0.1"

    /** */
    var resizable: Boolean = true

    /** */
    var debug: Boolean = true

    /** */
    lateinit var gameScene: GameScene

    /** */
    lateinit var gameApp: GameApp

    /** */
    lateinit var input: Input

    /**
     *
     */
    val readOnlySetting: ReadOnlySetting by lazy {
        ReadOnlySetting(this)
    }

    /**
     *
     */
    val gameWorld: World by lazy {
        World(this.readOnlySetting)
    }

    /** */
    val entityFactory: EntityFactory by lazy {
        EntityFactory()
    }

}