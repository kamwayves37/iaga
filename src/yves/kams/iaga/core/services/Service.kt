package yves.kams.iaga.core.services

/**
 * @author yves kams
 * @since 2020-06-8
 */
interface Service {

    /**
     *
     */
    fun onInit()

    /**
     * @param tps
     */
    fun onUpdate(tps: Double)

    /**
     *
     */
    fun onExit()
}