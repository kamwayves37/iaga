package yves.kams.iaga.core.pools

import kotlin.math.min

/**
 * @author yves kams
 * @since 2020-06-12
 */
class Pool(thread: Int) {

    /** */
    private val threads: Array<PoolThread>

    /** */
    private var activeThreads: Int = 0

    init {
        val initialLock = PoolLock(thread)
        threads = Array(thread) { PoolThread(initialLock) }
        initialLock.lock()
        setActiveThreads(thread)
    }

    /**
     * @param function
     * @param range
     */
    fun execute(range: Int, function: (Int) -> Any) {
        val cores = min(activeThreads, range)
        val lock = PoolLock(cores)
        for (i in 0 until cores) {
            val start = (i.toDouble() / cores * range).toInt()
            val end = ((i + 1).toDouble() / cores * range).toInt()

            threads[i].execute(PoolThreadRange(start, end), lock, function)
        }
        lock.lock()
    }


    /**
     * @param limit
     */
    fun setActiveThreads(limit: Int) {
        activeThreads = min(threads.size, limit)
    }

    /**
     * @return int
     */
    fun getActiveThreads(): Int {
        return activeThreads
    }

    /**
     * @return boolean
     */
    fun finished(): Boolean {
        for (thread in threads) {
            if (thread.isExecuting()) return false
        }
        return true
    }

    /**
     *
     */
    fun stop() {
        threads.forEach {
            it.interrupt()
        }
    }
}