package yves.kams.iaga.core.pools

/**
 * @author yves kams
 * @since 2020-06-12
 */
data class PoolThreadRange(val start: Int, val end: Int)