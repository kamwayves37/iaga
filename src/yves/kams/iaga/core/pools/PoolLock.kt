package yves.kams.iaga.core.pools


class PoolLock(private val threads: Int) {
    private var counter = 0
    private var isLocked = false

    private val lock = Object()

    fun lock() {
        if (counter >= threads) return
        synchronized(lock) {
            try {
                isLocked = true
                lock.wait()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }

    fun unlock() {
        synchronized(lock) {
            counter++
            if (counter >= threads) {
                lock.notify()
                isLocked = false
            }
        }
    }
}