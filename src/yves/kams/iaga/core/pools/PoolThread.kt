package yves.kams.iaga.core.pools

/**
 * @author yves kams
 * @since 2020-06-12
 */
class PoolThread(private val initialLock: PoolLock) : Thread("FX Pool Thread") {

    private val objLock = Object()
    private lateinit var function: (Int) -> Any
    private lateinit var range: PoolThreadRange
    private lateinit var lock: PoolLock
    private var executing = false

    init {
        start()
    }

    /**
     * @return boolean
     */
    fun isExecuting(): Boolean {
        return executing
    }

    fun execute(range: PoolThreadRange, lock: PoolLock, function: (Int) -> Any) {
        if (isExecuting()) throw RuntimeException("Thread is already executing")
        this.function = function
        this.range = range
        this.lock = lock

        synchronized(objLock) {
            objLock.notify()
        }
    }

    /**
     *
     */
    override fun run() {
        synchronized(objLock) {
            this.initialLock.unlock()
            while (!this.isInterrupted) {
                try {
                    objLock.wait()
                    executing = true
                    for (i in range.start until range.end) {
                        function.invoke(i)
                    }
                    executing = false
                    lock.unlock()
                } catch (e: InterruptedException) {
                    interrupt()
                }
            }
            interrupt()
        }
    }
}