package yves.kams.iaga.core.utils.generator

/**
 * @author yves kams
 * @since 2020-06-11
 */
class IDGenerator {

    companion object {
        /** */
        private var ID: Int = 0

        /**
         * @return int
         */
        fun generateID(): Int {
            return ID++
        }
    }
}