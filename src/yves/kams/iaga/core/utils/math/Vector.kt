package yves.kams.iaga.core.utils.math

/**
 * @author yves kams
 * @since 2020-06-8
 */
abstract class Vector {

    /**
     * @param other
     */
    private constructor(other: Vector) {
        TODO("")
    }

    /**
     *
     * @return vector
     */
    fun copy(): Vector {
        TODO("Not implement")
    }

    /**
     * @return vector
     */
    fun newInstance(): Vector {
        TODO("Not implement")
    }

    /**
     * @param index
     * @return float
     */
    operator fun get(index: Int): Float {
        TODO("Not implement")
    }

    /**
     * @param index
     * @param value
     */
    operator fun set(index: Int, value: Float) {
        TODO("Not implement")
    }

    /**
     * @param other
     * @return vector
     */
    fun add(other: Vector): Vector {
        TODO("Not implement")
    }

    /**
     * @param other
     * @return vector
     */
    fun sub(other: Vector): Vector {
        TODO("Not implement")
    }

    /**
     * @param other
     * @return vector
     */
    fun mul(other: Vector): Vector {
        TODO("Not implement")
    }

    /**
     * @param other
     * @return vector
     */
    fun div(other: Vector): Vector {
        TODO("Not implement")
    }

    /**
     * @param other
     * @return vector
     */
    fun dot(other: Vector): Vector {
        TODO("Not implement")
    }

    /**
     * @return matrix
     */
    fun fromMatrix(): Matrix{
        TODO("Not implement")
    }

    /**
     * @param value
     * @return vector
     */
    fun add(value: Float): Vector {
        TODO("Not implement")
    }

    /**
     * @param value
     * @return vector
     */
    fun sub(value: Float): Vector {
        TODO("Not implement")
    }

    /**
     * @param value
     * @return vector
     */
    fun mul(value: Float): Vector {
        TODO("Not implement")
    }

    /**
     * @param value
     * @return vector
     */
    fun div(value: Float): Vector {
        TODO("Not implement")
    }

    /**
     * Use to display vector values on standard output
     */
    fun printArray() {
        TODO("Not implement")
    }

    companion object{

        /**
         *
         */
        fun map(){

        }
    }
}