package yves.kams.iaga.core.utils.math

/**
 * @author yves kams
 * @since 2020-06-8
 */
class Matrix(val rows: Int, val cols: Int) {

    /**
     * Copy builder
     * @param other copy matrix
     */
    constructor(other: Matrix): this(other.rows, other.cols) {
        TODO("Not implement")
    }

    /**
     * Make a copy of the current matrix
     * @return new instance of the matrix
     */
    fun copy(): Matrix {
        TODO("Not implement")
    }

    /**
     * A new instance of the current matrix
     * with the same number of rows and cols.
     * @return matrix
     */
    fun newInstance(): Matrix {
        TODO("Not implement")
    }

    /**
     * @param row The row position
     * @param col The col position
     * @return float This returns the value to position [row,col]
     */
    operator fun get(row: Int, col: Int): Float {
        TODO("Not implement")
    }

    /**
     * @param row The row position
     * @param col The col position
     * @param value The value to be put at the position [row,col]
     */
    operator fun set(row: Int, col: Int, value: Float) {
        TODO("Not implement")
    }

    /**
     * @return int This size of matrix
     */
    fun size(): Int {
        TODO("Not implement")
    }

    /**
     * @return matrix Transpose
     */
    fun transpose(): Matrix {
        TODO("Not implement")
    }

    /**
     * Addition between two matrices
     * The matrices must be of the same type
     * @param other The matrix to be add
     * @return matrix The result of the addition
     * @exception error if the matrices are not of the same order
     */
    fun add(other: Matrix): Matrix {
        TODO("Not implement")
    }

    /**
     * Subtraction between two matrices
     * The matrices must be of the same type
     * @param other The matrix to be subtraction
     * @return matrix The result of the subtraction
     * @exception error if the matrices are not of the same order
     */
    fun sub(other: Matrix): Matrix {
        TODO("Not implement")
    }

    /**
     * Multiplication between two matrices
     * The matrices must be of the same type
     * @param other The matrix to be multiply
     * @return matrix The result of the Multiplication
     * @exception error if the matrices are not of the same order
     */
    fun mul(other: Matrix): Matrix {
        TODO("Not implement")
    }

    /**
     * Division between two matrices
     * The matrices must be of the same type
     * @param other The matrix to be divided
     * @return matrix The result of the Division
     * @exception error if the matrices are not of the same order
     */
    fun div(other: Matrix): Matrix {
        TODO("Not implement")
    }

    /**
     *
     * @param other
     */
    fun dot(other: Matrix): Matrix {
        TODO("Not implement")
    }

    /**
     * Convert a matrix into a float array
     * @return float[] The result of the conversion
     */
    fun toArray(): FloatArray {
        TODO("Not implement")
    }

    /**
     * Transforms the matrix into a vector array
     * @return vector The result of the transformation
     */
    fun toVector(): Vector {
        TODO("Not implement")
    }


    /**
     * Adds a value to all elements of the matrix
     * @param value The value to be add
     * @return matrix The result of the scalar addition
     */
    fun add(value: Float): Matrix {
        TODO("Not implement")
    }

    /**
     * Division a value to all elements of the matrix
     * @param value The value used as divisor
     * @return matrix The result of the scalar division
     */
    fun div(value: Float): Matrix {
        TODO("Not implement")
    }

    /**
     * Multiplication a value to all elements of the matrix
     * @param value The value to be multiply
     * @return matrix The result of the scalar multiplication
     */
    fun mul(value: Float): Matrix {
        TODO("Not implement")
    }

    /**
     * Use to display matrix values on standard output
     */
    fun printArray() {
        TODO("Not implement")
    }

    companion object {
        /**
         * @param row
         * @param col
         * @return
         */
        fun randomize(row: Int, col: Int): Matrix {
            TODO("Not implement")
        }

        /**
         * Convert a vector array into into a matrix
         * @param array The array to be converted
         * @return
         */
        fun fromArray(array: FloatArray): Matrix {
            TODO("Not implement")
        }

        /**
         * Use to browse through each value of the matrix
         */
        fun map() {
            TODO("Not implement")
        }
    }
}