package yves.kams.iaga.core.utils.models

/**
 * @author yves kams
 * @since 2020-06-7
 */
open class GenericMap<T, V> {

    /** */
    private val map: MutableMap<T, V> = mutableMapOf()

    val size: Int = map.size

    /**
     * @param type
     * @param value
     */
    operator fun set(type: T, value: V) {
        map[type] = value
    }

    /**
     * @param type
     * @return V
     */
    operator fun get(type: T): V {
        TODO("Not implement")
    }

    /**
     * @param type
     */
    fun remove(type: T): Boolean {
        TODO("Not implement")
    }

    /**
     */
    fun removeAll() {
        TODO("Not implement")
    }

    /**
     * @param type
     */
    fun has(type: T): Boolean {
        TODO("Not implement")
    }

    /**
     */
    fun types(): HashSet<T> {
        TODO("Not implement")
    }
}