package yves.kams.iaga.core.utils.reflection

/**
 * @author yves kams
 * @since 2020-06-12
 */
class Reflection {
    companion object {
        /**
         * @param type
         */
        fun <T> newInstance(type: Class<T>): T {
            try {
                return type.getConstructor().newInstance()
            } catch (e: Exception) {
                throw IllegalArgumentException(e.message + " for Class " + type.simpleName + " because the constructor is not the default constructor.")
            }
        }
    }
}