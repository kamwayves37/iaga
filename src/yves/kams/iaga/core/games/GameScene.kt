package yves.kams.iaga.core.games

import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.text.Text
import javafx.stage.Stage
import yves.kams.iaga.core.settings.Setting

/**
 * @author yves kams
 * @since 2020-06-8
 */
class GameScene(private val stage: Stage, private val setting: Setting) {
    private val root: Group = Group()
    private val text: Text = Text()
    private lateinit var canvas: Canvas
    private lateinit var scene: Scene

    init {
        initCanvas()
        initText()
        initScene()
        initStage()

        /**
         *
         */
        setting.gameScene = this
    }

    /**
     *
     */
    private fun initScene() {
        this.scene = Scene(this.root)
    }

    /**
     *
     */
    private fun initStage() {
        stage.isResizable = setting.resizable

        stage.title = "${setting.title} v${setting.version}"
        stage.scene = this.scene
    }

    /**
     *
     */
    private fun initCanvas() {
        this.canvas = Canvas(setting.width, setting.height)

        root.children += this.canvas
    }

    /**
     *
     */
    private fun initText() {
        text.text = "Debug Game"
        text.style = "fx-font-size: 11pt; -fx-font-smoothing-type: lcd; -fx-fill: #ff3c0e;"
        text.translateX = 10.0
        text.translateY = (setting.height - 100)

        this.root.children += text
    }

    /**
     * @return graphicsContext
     */
    fun getGraphicContext(): GraphicsContext {
        return this.canvas.graphicsContext2D
    }

    /**
     * @return scene
     */
    fun getScene(): Scene {
        return this.scene
    }

    /**
     * @return text
     */
    fun getText(): Text {
        return this.text
    }

    /**
     * @return group
     */
    fun getRoot(): Group {
        return this.root
    }

    /** Displays the window */
    fun show() {
        stage.show()
    }
}