package yves.kams.iaga.core.games

import javafx.application.Application
import javafx.beans.value.ObservableValue
import javafx.scene.canvas.GraphicsContext
import javafx.stage.Stage
import yves.kams.iaga.core.concurrent.Async
import yves.kams.iaga.core.concurrent.IOTask
import yves.kams.iaga.core.ecs.entities.EntityFactory
import yves.kams.iaga.core.engine.GameEngine
import yves.kams.iaga.core.inputs.Input
import yves.kams.iaga.core.services.Service
import yves.kams.iaga.core.settings.ReadOnlySetting
import yves.kams.iaga.core.settings.Setting

/**
 * @author yves kams
 * @since 2020-06-15
 */
abstract class GameApp : Application() {

    /** */
    private val setting: Setting = Setting()

    /** */
    private lateinit var gameScene: GameScene

    /** */
    private lateinit var gameEngine: GameEngine

    /**
     *
     */
    final override fun init() {
        initSettings(this.setting)

        /** */
        this.setting.gameApp = this
    }

    /**
     *
     */
    fun getSetting(): ReadOnlySetting {
        return this.setting.readOnlySetting
    }

    /**
     *
     */
    final override fun start(stage: Stage) {
        gameScene = GameScene(stage, this.setting)

        gameEngine = GameEngine(this.setting)

        stage.iconifiedProperty().addListener { _: ObservableValue<out Boolean>?, _: Boolean?, t1: Boolean ->
            if (t1) {
                gameEngine.pause()
            } else {
                gameEngine.resume()
            }
        }

        gameScene.show()

        //Async
        val task = IOTask.ofVoid {
            initSystems()
        }.onSuccess {
            gameEngine.start()
        }.onFailure {
            it.printStackTrace()
        }.toJavaFXTask()

        Async.INSTANCE.execute(task)
    }

    /**
     *
     */
    final override fun stop() {
        gameEngine.stop()
        Async.INSTANCE.shutdownNow()
    }

    /**
     * @param setting
     */
    protected abstract fun initSettings(setting: Setting)

    /**
     * Allows you to define the different
     * systems that the world needs
     * to interact with entities through
     * its components.
     */
    protected abstract fun initSystems()

    /**
     * Allows to define the different
     * types of entities in the world
     * @param factory entities management layer
     */
    protected abstract fun initEntities(factory: EntityFactory)

    /**
     *
     */
    protected abstract fun initPlayerControls(input: Input)

    /**
     *
     */
    protected abstract fun initGames()

    /**
     *
     */
    protected open fun onUpdate(tps: Double) {}

    /**
     *
     */
    protected open fun onDraw(gc: GraphicsContext) {}

    /**
     *
     */
    protected open fun onExit() {}

    /**
     *
     */
    internal class GameService(private val app: GameApp) : Service {

        override fun onInit() {
            app.initEntities(app.setting.entityFactory)
            app.initPlayerControls(app.setting.input)
            app.initGames()
        }

        override fun onUpdate(tps: Double) {
            app.onUpdate(tps)
        }

        override fun onExit() {
            app.onExit()
        }
    }
}
